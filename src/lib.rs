use std::fs;
use std::collections::HashMap;
use pest::{Parser, iterators::Pair};
use pest_derive::Parser;

#[derive(Parser, Debug)]
#[grammar = "webvtt.pest"]
pub struct WebVTT;

pub struct Cue {
    pub timing:
}

pub struct WebVTTData {
    pub header_comment: Option(&str),
    pub properties: Vec<HashMap<&str, &str>>,
    //align: Option(&str),
    pub cues: Vec<Cue>,
}

impl WebVTTData {
    pub fn parse(&Self, file_contents: String) -> Self {
        let mut file = WebVTT::parse(Rule::file, &file_contents).expect("Parsing failed");

        Self {

        }
    }
}
