use pest::{Parser, iterators::Pair};
use pest_derive::Parser;

use std::collections::HashMap;
use std::fs;

#[derive(Parser, Debug)]
#[grammar = "webvtt.pest"]
pub struct WebVTT;

fn main() {
    let unparsed_file = fs::read_to_string("tests/basic.vtt").expect("cannot read file");

    let mut file = WebVTT::parse(Rule::file, &unparsed_file)
        .expect("parsing failed");

    let mut _properties: HashMap<&str, HashMap<&str, &str>> = HashMap::new();

    for line in file {
        println!("{:?}", line.as_rule());
        match line.as_rule() {
            Rule::HEADER => {
                let _comment = line.as_str();
            },
            Rule::properties => {
                let mut inner_rules = line.into_inner();
                let key = inner_rules.next().unwrap().as_str();
                let value = inner_rules.next().unwrap().as_str();
                println!("{}: {}", key, value);
            },
            Rule::cues => {
                let mut inner_rules = line.into_inner();

                for cue_data in inner_rules {
                    println!("{:?}", cue_data.as_rule());
                    match cue_data.as_rule() {
                        Rule::timing => {
                            let timing = cue_data.into_inner();
                            // let start = timing.next().unwrap();
                            // let end = timing.next().unwrap();

                            for time in timing {
                                match time.as_rule() {
                                    Rule::start => {
                                        let start_times = time.into_inner();
                                        for val in start_times {
                                            match val.as_rule() {
                                                Rule::hour => {

                                                },
                                                Rule::minute => {

                                                },
                                                Rule::second => {

                                                },
                                                Rule::millisecond => {

                                                },
                                                _ => unreachable!(),
                                            }
                                        }
                                    },
                                    Rule::end => {
                                        let end_times = time.into_inner();
                                        for val in end_times {
                                            match val.as_rule() {
                                                Rule::hour => {

                                                },
                                                Rule::minute => {

                                                },
                                                Rule::second => {

                                                },
                                                Rule::millisecond => {

                                                },
                                                _ => unreachable!(),
                                            }
                                        }
                                    },
                                    _ => unreachable!(),
                                }
                            }

                        },
                        Rule::align => {
                            let align = cue_data.as_str();
                            println!("align:{}", align);
                        },
                        Rule::position => {
                            let align = cue_data.as_str();
                            println!("position:{}", align);
                        },
                        Rule::payload => {
                            let align = cue_data.as_str();
                            println!("payload:{}", align);
                        },
                        _ => unreachable!(),
                    }
                }
            },
            Rule::EOI => (),
            _ => unreachable!(),
        }
    }
}
